const moment = require('moment');
let botid = "438738610909609984";
let CurrentDate = moment().toISOString();
let knex = require('knex')({
  client: 'mysql', //process.env.DBCLIENT
  connection: {

  }
});


let self = module.exports = {

  start: async function(type, userid) {

    knex.select('owner', 'playercount', 'id').from('groups').where('owner', userid).then(function(data) {

      console.log("Group Select Data: ")
      console.log(data);

      if (data.length > 0) {
        return 'alreadyingroup';
      } else {
        knex('groups').insert({
          type: type,
          owner: userid,
          playercount: 1
        }).then(function(response) {
          if (response === 1) {
            return data.id;
          } else {
            return 'insertfail';
          }
        });
      }
    });
  },
  play: async function(group, userid) {
    knex.select('id').from('queue').where('playerid', userid).then(function(data) {
      console.log(data);
      if (data.length > 0) {
        msg.reply('You are already in the queue!')
      } else {
        knex('queue').insert({
            playerid: userid,
            playername: username,
            time: CurrentDate,
            group: group
          })
          .then(function(result) {
            console.log(result)
            if (result === 1)
              msg.reply('You are now added to the queue:');
            else
              msg.reply('Adding to queue failed. Please try again.')
          });
      }
    });
  },
  drop: async function(userid) {
    console.log("trying to delete: " + userid);
    knex('queue')
      .where('playerid', userid)
      .del().then(function(result) {
        console.log("Delete result: " + result);
        return result;
      });
  },
  list: async function(groupNumber) {
    knex.select('playerid', 'playername', 'position', 'time').from('queue').then(function(data) {
      let returnString = "";
      _.forEach(data, function(singleItem) {
        let playerName = singleItem.playername;
        let playerDate = singleItem.time;
        let position = singleItem.position;
        returnString += playerName + " queued at " + playerDate + "\n";
      });
      return returnString;
    });
  },

  deleteGroup: async function() {

  },
  adminDeleteAllGroups: async function() {

  },
  adminDropPlayer: async function() {

  },




}